from chiplotle import *
import sqlite3
import chiplotle
from time import sleep
from sys import stderr

conn = sqlite3.connect("plotter.db")
conn.row_factory = sqlite3.Row

plotter = instantiate_plotters()[0]

plotter.write ('IN')

while True:
	commands = []
	
	for command in conn.execute ('SELECT c.command, c.id FROM commands c JOIN sessions s ON s.id = c.session WHERE s.active = 1 AND c.done = 0 ORDER BY c.timestamp ASC').fetchall():
		try:
			conn.execute ('UPDATE commands SET done = 1 WHERE id = :id', {'id': int (command['id'])})
			conn.commit()
			commands.append (command['command'])
		except:
			stderr.write ("Couldn't mark command as done. Database locked?")
			
	try:
		plotter.write ('%s' % str (';'.join(commands)))
		print '%s' % str (';'.join(commands))
	except:
		stderr.write ("Couldn't write to plotter")
	
	sleep (2)