SELECT c.command FROM commands c JOIN sessions s ON s.id = c.session WHERE s.active = 1 AND c.done = 0 ORDER BY timestamp ASC

UPDATE commands SET done = 1 WHERE id = ?

INSERT INTO sessions (active) VALUES(1)
INSERT INTO commands (command,session) VALUES('PD1000,2000,2000,2000;PU',1)

CREATE TABLE commands (
    id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    command TEXT,
    session INTEGER,
	top INTEGER,
	right INTEGER,
	bottom INTEGER,
	left INTEGER,
    done INTEGER DEFAULT 0,
    timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE sessions (
    id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
    active INTEGER DEFAULT 0
);
